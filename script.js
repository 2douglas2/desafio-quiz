//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    //pergunta extra
    {
        texto: 'Qual o nome do mascote da GTi',
        respostas: [
            {valor: 1, texto: 'Foguetão'},
            {valor: 3, texto: 'Kleyton'},
            {valor: 2, texto: 'Stitch'},
            {valor: 0, texto: 'Ricardo'}
        ]
    }
];
// Variável que recebe a identificação da questão
var questao=0;

//Variável que recebe a nota
var nota=0;

//Função inicia o quiz e muda a questão
function mostrarQuestao() {
    
    //Calcula a nota de acordo com a questão dada
    for(var i=0; i<4; i++){
        if (questao != 0 && document.getElementsByName ("resposta")[i].checked){
            nota = nota + perguntas[questao-1].respostas[i].valor;
        }
    }

    //Finaliza o Quiz caso seja a ultima prgunta
    if(questao==6){
        finalizarQuiz();
    }
    //Verifica se é uma questão ao o inicio e se a questão foi respondida, e altera a questão
    else if(questao == 0 || document.getElementsByName ("resposta")[0].checked || document.getElementsByName ("resposta")[1].checked || document.getElementsByName ("resposta")[2].checked || document.getElementsByName ("resposta")[3].checked){
        
        //Mostra a questão
        document.getElementById("titulo").innerHTML = perguntas[questao].texto;

        //Altera o botão para proxima
        document.getElementById("confirmar").innerHTML = "PROXIMA";

        //Deixa os itens visivel 
        document.getElementById("listaRespostas").style.display = "inline";

        //Oculta o resultado caso o quiz tenha sido reiniciado
        document.getElementById("resultado").style.display = "none";
        
        // Escreve as questões e desmarca as opções
        for(var i=0; i<4; i++){
            document.getElementsByTagName("span")[i].innerHTML = perguntas[questao].respostas[i].texto;
            document.getElementsByName ("resposta")[i].checked = false;
        }
        
        //A variável de identificação da questão soma +1
        questao++;     
    }
}

// Fução para finalizar Quiz
function finalizarQuiz() {

    //Altera o titulo 
    document.getElementById("titulo").innerHTML = "QUIZ DOS VALORES DA GTI";

    //Altera o botão para Refazer quiz
    document.getElementById("confirmar").innerHTML = "Refazer Quiz";

    //Oculta a lista de itens
    document.getElementById("listaRespostas").style.display = "none";

    //Variável resultado recebe a porcentágem da nota
    var resultado = (nota/18)*100;

    //Deixa o resultado visivel
    document.getElementById("resultado").style.display = "block";

    //Mostra o resultado obitido em porcentagem 
    document.getElementById("resultado").innerHTML = "Sua pontuação: " + resultado.toFixed(0) + "%";

    //identificação da questão recebe 0 para reiniciar o Quiz
    questao = 0;

    //Nota recebe 0 para reiniciar o quiz
    nota = 0;
}